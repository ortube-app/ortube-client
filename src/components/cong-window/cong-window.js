import React, {Component} from "react";
import "./cong-window.css";
import {withBody} from "../hoc-helpers";
import {withRouter} from "react-router-dom";
import apiClient from "../../services/apiClient";

class CongWindow extends Component {

    API = new apiClient();

    activeUser = () => {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const code = urlParams.get('code');
        if (code) this.API.activate(localStorage.getItem('accessToken'), code);
    };

    render() {
        this.activeUser();

        return (
            <div className="con">
                <div className="img"></div>
                <h2>Now you can use our site!</h2>
            </div>
        )
    }
}

export default withRouter(withBody(CongWindow, 8));
