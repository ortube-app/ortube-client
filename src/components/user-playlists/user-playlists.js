import React, {Component} from "react";
import "./user-playlists.css";
import ApiClient from "../../services/apiClient";
import apple from "../../images/apple.jpg";
import {withBody} from "../hoc-helpers";
import {Link} from "react-router-dom";

class UserPlaylists extends Component {

    state = {
        playlists: [],
        needReload: false,
        loading: true,
    };

    API = new ApiClient();

    constructor(props) {
        super(props);
        this.updatePlaylists();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.needReload !== this.state.needReload) {
            this.setState({
                needReload: false,
            });
            this.updatePlaylists();
        }
    }

    onAddPlaylist = () => {
        const l1 = document.getElementById("int1").value;
        const l2 = document.getElementById("int2").value;
        this.updatePlaylists();
        if (this.state.accessToken !== null) this.API.createUserPlaylist("PUBLIC", l1, l2, localStorage.getItem('accessToken'));
        this.setState({
            needReload: true,
        });
        this.updatePlaylists();
    };

    updatePlaylists() {

        this.API
            .getAllUsersPlaylists(localStorage.getItem('accessToken'))
            .then((playlists) => {
                this.setState({
                    playlists: playlists,
                    loading: false,
                });
            });
    }


    render() {

        const add = localStorage.getItem('user') ? <div className="cell" key="add">
            <img alt="apple" src={apple}/>
            <div className="flex-col-con">
                <div className="title"><input placeholder="Enter playlist name" id="int1"/></div>
                <div className="topic"><input placeholder="Enter playlist topic" id="int2"/></div>
                <div className="words">
                    <button onClick={this.onAddPlaylist}>Create</button>
                </div>
            </div>
        </div> : null;

        const content = this.state.loading ? <div className="loadingio-spinner-wedges-1kn05ey5fav">
            <div className="ldio-y4vhssos6yl">
                <div>
                    <div>
                        <div></div>
                    </div>
                    <div>
                        <div></div>
                    </div>
                    <div>
                        <div></div>
                    </div>
                    <div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div> : (
            <div className="flex-container">
                {add}

                {this.state.playlists.map((playlist) => {
                    return (
                        <div className="cell" key={playlist.id}>
                            <img alt="apple" src={apple}/>
                            <div className="flex-col-con">
                                <Link to={`/user/${localStorage.getItem('user')}/${playlist.id}`}>
                                    <div className="title">{playlist.title}</div>
                                </Link>
                                <div className="topic">{playlist.topic}</div>
                                <div className="words">
                                    <ul>
                                        {playlist.words.map((word, index) => {
                                            return (
                                                <li key={index}>{word.lang1} - {word.lang2}</li>
                                            )
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    )
                })}


            </div>
        );

        return (
            <React.Fragment>
                {content}
            </React.Fragment>
        );
    }
}

export default withBody(UserPlaylists, 1);
