import React, {Component} from "react";
import "./loginForm.css";
import ApiClient from "../../services/apiClient";
import {withBody} from "../hoc-helpers";


class LoginForm extends Component {

    state = {
        message: null,
    };

    API = new ApiClient();

    validate = () => {
        const user = document.getElementById('user').value;
        const pass = document.getElementById('pass').value;

        this.API.login({"password": pass, "username": user}, this.props.hash)
            .then((body) => {
                if (body.message) {
                    this.setState({
                        message: body.message,
                    })
                } else {
                    localStorage.setItem('accessToken', body.accessToken);
                    localStorage.setItem('role', body.role);
                    this.API.me(body.accessToken).then((body) => localStorage.setItem('user', body.username));
                    setTimeout(() => window.location.href = '/playlists/', 300);
                }
            });

    };


    render() {

        return (
            <React.Fragment>
                <div id="login-box">
                    <div className="left">
                        <h1>Sign in</h1>

                        <input type="text" id="user" name="username" placeholder="Username"/>
                        {/*<input type="text" name="email" placeholder="E-mail"/>*/}
                        <input type="password" id="pass" name="password" placeholder="Password"/>
                        {/*<input type="password" name="password2" placeholder="Retype password"/>*/}
                        <div className="error"> {this.state.message} </div>
                        <input onClick={this.validate} type="submit" name="signup_submit" value="Sign me in"/>
                    </div>

                    <div className="right">
                        <span className="loginwith">Sign in with<br/>social network</span>

                        <button className="social-signin facebook">Log in with facebook</button>
                        <button className="social-signin twitter">Log in with Twitter</button>
                        <button className="social-signin google">Log in with Google+</button>
                    </div>
                    <div className="or">OR</div>
                </div>
            </React.Fragment>
        );
    }
}

export default withBody(LoginForm, 2);
