import React from "react";
import "./exit.css";
import {Redirect} from "react-router-dom";

const Exit = () => {
    localStorage.clear();

    return (
        <Redirect to="/playlists/"/>
    );
};

export default Exit;