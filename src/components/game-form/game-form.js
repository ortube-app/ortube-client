import React, {Component} from "react";
import "./game-form.css";
import {withBody} from "../hoc-helpers";
import {withRouter, Link} from "react-router-dom";


class GameForm extends Component {

    render() {
        return (
            <React.Fragment>
                <form className="forms">
                    <label>Select options for playlist "{localStorage.getItem('playlistTitle')}"</label>
                    <label className="form-l" htmlFor="imageMode">Select image-mode</label>
                    <select className="form-s" id="imageMode">
                        <option>Without image</option>
                        <option>With image</option>
                    </select>
                    <label className="form-l" htmlFor="languageMode">Select language-mode</label>
                    <select className="form-s" id="languageMode">
                        <option>Lang 1 - Lang 2</option>
                        <option>Lang 2 - Lang 1</option>
                        <option>Mixed</option>
                    </select>
                </form>
                <Link to='/game/'>
                    <button className="form-b">Play!</button>
                </Link>
            </React.Fragment>
        )
    }
}

export default withRouter(withBody(GameForm, 8));
