import React, {Component} from "react";

const withBody = (View, type) => {
    return class extends Component{
            render() {
                return (
                    <div className="body">
                        <div className={`active-content myRow${type}`}>
                            <View {...this.props}/>
                        </div>
                    </div>
                )
            }
        }
};

export default withBody;