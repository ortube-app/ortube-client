import React, {Component} from "react";
import "./app.css";
import Header from "../header";
import PlaylistsPage from "../playlistsPage";
import Exit from "../exit";
import {BrowserRouter as Router, Route, Redirect, Switch, useLocation} from "react-router-dom";
import Playlist from "../playlist";
import Fingerprint2 from "fingerprintjs2";
import Login from "../login";
import LoginForm from "../loginForm/loginForm";
import UserPlaylists from "../user-playlists/user-playlists";
import UserPlaylist from "../user-playlist/user-playlist";
import RegisterForm from "../registerForm/registerForm";
import CongWindow from "../cong-window/cong-window";
import GameForm from "../game-form/game-form";
import Game from "../game/game";


class App extends Component {

    state = {
        hash: null,
        showSite: false,
    };

    timer;

    updateHash = () => {
        Fingerprint2.getPromise({}).then((components) => {
            let values = components.map(function (component) {
                return component.value
            });
            return Fingerprint2.x64hash128(values.join(''), 31);
        }).then((hash) => {
            this.setState({hash: hash});
        });
    };

    componentDidMount() {
        this.timer = setTimeout(() => {
            this.setState({showSite: true})
        }, 500);

        //console.log(this.timer);
        this.updateHash();
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.hash !== this.state.hash) {
            this.updateHash();
        }
    }

    render() {
        const show = !this.state.showSite ? <div>Hi</div> : null;
        const content = this.state.showSite ? <Content hash={this.state.hash}/> : null;

        return (
            <Router>
                {show}
                {content}
            </Router>
        );
    }
}

const Content = ({hash}) => {
    let location = useLocation();
    //console.log(location.pathname);
    return (
        <Switch>
            <React.Fragment>
                <div className="app">
                    <Redirect to={location.pathname}/>
                    <Header/>
                    <Route path="/" exact>
                        <Redirect to="/playlists/"/>
                    </Route>

                    <Route path="/playlists/" exact children={<PlaylistsPage hash={hash}/>}/>
                    <Route path="/playlists/:id" children={<Playlist hash={hash}/>}/>
                    <Route path="/user/:id" exact children={<UserPlaylists hash={hash}/>}/>
                    <Route path="/user/:id/:id" children={<UserPlaylist hash={hash}/>}/>
                    <Route path="/account/" exact>
                        <h1>Account</h1>
                        <h1>Account</h1>
                        <h1>Account</h1>
                    </Route>
                    <Route path="/faq/">
                        <h1>FAQ</h1>
                        <h1>FAQ</h1>
                        <h1>FAQ</h1>
                    </Route>
                    <Route path="/activate/" exact children={<CongWindow hash={hash}/>}/>
                    <Route path="/play/">
                        <h1>Play</h1>
                        <h1>Play</h1>
                        <h1>Play</h1>
                    </Route>
                    <Route path="/exit/" children={<Exit hash={hash}/>}/>
                    <Route path="/game-form/" children={<GameForm hash={hash}/>}/>
                    <Route path="/game/" children={<Game hash={hash}/>}/>
                    <Route path="/login/" children={<Login hash={hash}/>}/>
                    <Route path="/loginForm/" children={<LoginForm hash={hash}/>}/>
                    <Route path="/registerForm/" children={<RegisterForm hash={hash}/>}/>
                </div>
            </React.Fragment>
        </Switch>
    );
};

export default App;
