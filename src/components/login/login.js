import React from "react";
import "./login.css";
import {Redirect} from "react-router-dom";
import ApiClient from "../../services/apiClient";

const Login = (props) => {
    const API = new ApiClient();
    API.login({"password": "Password@123", "username": "alxmute"}, props.hash).then((body) => {
        localStorage.setItem('accessToken', body.accessToken);
        localStorage.setItem('role', body.role);
    });

    return (
        <Redirect to="/playlists/"/>
    );
};

export default Login;