import React, {Component} from "react";
import "./playlist.css";
import ApiClient from "../../services/apiClient";
import {withBody} from "../hoc-helpers";
import {withRouter} from "react-router-dom";

class Playlist extends Component{

    state = {
      playlist: {
          words: [],
      },
        needReload: false,
        error: false,
    };

    API = new ApiClient();


    onUpdatePlaylist = () => {
        const title = document.getElementById("title");
        const topic = document.getElementById("topic");
        const inTitle = document.getElementById("title1");
        const inTopic = document.getElementById("topic1");
        const button = document.getElementById("updateButton");
        inTitle.value = this.state.playlist.title;
        inTopic.value = this.state.playlist.topic;
        title.style.visibility = "hidden";
        topic.style.visibility = "hidden";
        inTopic.style.visibility = "visible";
        inTitle.style.visibility = "visible";
        button.style.visibility = "visible";
    };

    onUpdateWord = (wordId) => {
        return () => {
            const wordName = document.getElementById(`wordName${wordId}`);
            const btnWord = document.getElementById(`btnWord${wordId}`);
            const inWordName = document.getElementById(`wordName${wordId}1`);
            btnWord.style.visibility = 'visible';
            wordName.style.visibility = "hidden";
            inWordName.style.visibility = "visible";
            const wordObj = this.state.playlist.words.find(({id}) => id === wordId);
            inWordName.value = `${wordObj.lang1} - ${wordObj.lang2}`;
        };
    };

    onUpdateWordWithData = (wordId) => {
        return () => {
            const inWordName = document.getElementById(`wordName${wordId}1`);
            const btnWord = document.getElementById(`btnWord${wordId}`);
            btnWord.style.visibility = 'hidden';
            let data = inWordName.value.split('-');
            data[0] = data[0].trim();
            data[1] = data[1].trim();
            const finalData = {
                "lang1": data[0],
                "lang2": data[1]
            };
            if (localStorage.getItem('accessToken') !== null) this.API.updateWord(this.state.playlist.id, finalData, localStorage.getItem('accessToken'), wordId);
            const wordName = document.getElementById(`wordName${wordId}`);
            wordName.style.visibility = "visible";
            inWordName.style.visibility = "hidden";
            setTimeout(() => {
                this.updatePlaylist(this.state.playlist.id);
            }, 200);
        };
    };

    onUpdatePlaylistWithData = () => {
        const title = document.getElementById("title");
        const topic = document.getElementById("topic");
        const inTitle = document.getElementById("title1");
        const inTopic = document.getElementById("topic1");
        const button = document.getElementById("updateButton");
        const data = {
            "title": inTitle.value,
            "topic": inTopic.value
        };
        if (localStorage.getItem('accessToken') !== null) this.API.updatePlaylist(this.state.playlist.id, data, localStorage.getItem('accessToken'));
        title.style.visibility = "visible";
        topic.style.visibility = "visible";
        inTopic.style.visibility = "hidden";
        inTitle.style.visibility = "hidden";
        button.style.visibility = "hidden";
        setTimeout(() => {
            this.updatePlaylist(this.state.playlist.id);
        }, 200);
    };

    onDeletePlaylist = () => {
        if (localStorage.getItem('accessToken') !== null) this.API.deletePlaylist(this.state.playlist.id, localStorage.getItem('accessToken'));
        setTimeout(() => {
            this.props.history.push('/playlists/');
        }, 500);
    };

    onDelete = (id) => {
        return () => {
            this.updatePlaylist(this.state.playlist.id);
            if (localStorage.getItem('accessToken') !== null && this.state.playlist.words.length > 0) this.API.deleteWordFromPlaylist(id, this.state.playlist.id, localStorage.getItem('accessToken'));
            this.setState({
                needReload: true,
            });
            this.updatePlaylist(this.state.playlist.id);
        };
    };

    addToUser = () => {
        this.API.addUserPlaylist(this.state.playlist.id, localStorage.getItem('accessToken'));
    };

    onDeleteSeveral = () => {
        let checkboxes = Array.from(document.getElementsByClassName('checkboxes'));
        let trueCheck = checkboxes.filter((el) => el.checked === true);
        let indexes = trueCheck.map((el) => {
            let str = el.className.split(' ')[0];
            let index = str[str.length - 1];
            return +index;
        });
        let wordsIds = indexes.map((id) => this.state.playlist.words[id].id);
        this.updatePlaylist(this.state.playlist.id);
        if (localStorage.getItem('accessToken') !== null) this.API.deleteSeveralWordsFromPlaylist(wordsIds, this.state.playlist.id, localStorage.getItem('accessToken'));
        let newCheckboxes = Array.from(document.getElementsByClassName('checkboxes'));
        newCheckboxes.forEach((el) => {
            el.checked = false;
        });
        this.setState({
            needReload: true,
        });
        this.updatePlaylist(this.state.playlist.id);
    };

    onAdd = () => {
        const l1 = document.getElementById("in1").value;
        const l2 = document.getElementById("in2").value;
        this.updatePlaylist(this.state.playlist.id);
        if (localStorage.getItem('accessToken') !== null) this.API.postWordToPlaylist({
            "lang1": l1,
            "lang2": l2
        }, this.state.playlist.id, localStorage.getItem('accessToken'));
        this.setState({
            needReload: true,
        });
        this.updatePlaylist(this.state.playlist.id);
    };

    componentDidMount() {
        window.scrollTo(0, 0);
        const id = this.props.match.params.id;
        this.updatePlaylist(id);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.needReload !== this.state.needReload) {
            this.setState({
                needReload: false,
            });
            this.updatePlaylist(this.state.playlist.id);
        }
    }

    onError = (error) => {
        console.log(`I fetch error ${error}`);
        this.setState({error: true});
    };

    updatePlaylist(id) {
        //console.log(this.props.hash);

        this.API
            .getPlaylist(id)
            .then((playlist) => {
                this.setState({
                   playlist: playlist,
                });
            })
            .catch(this.onError);
    }

    render() {
        const {title, words, topic, image} = this.state.playlist;
        const array = !words ? [] : words;
        const addToUser = localStorage.getItem('user') ? (
            <button onClick={this.addToUser}><i className="fas fa-plus-circle"></i></button>) : null;
        const controlPanel = localStorage.getItem('role') === 'ADMIN' ? (
            <React.Fragment>
                <button onClick={this.onDeletePlaylist}><i className="fas fa-trash-alt"></i></button>
                <button onClick={this.onUpdatePlaylist}><i className="fas fa-edit"></i></button>
            </React.Fragment>
        ) : null;

        const deleteUpdate = localStorage.getItem('user') ? (<div className="btns-group">
            {controlPanel}
            {addToUser}
        </div>) : null;


        const form = localStorage.getItem('role') === 'ADMIN' ? (<div className="form">
            <input className="wordLang1" id="in1" placeholder="Enter word 1"/>
            <input className="wordLang2" id="in2" placeholder="Enter word 2"/>
            <button type="button" onClick={this.onAdd} className="btn1"><i
                className="far fa-arrow-alt-circle-up"/></button>
            <button type="button" onClick={this.onDeleteSeveral} className="btn2"><i
                className="fas fa-trash-alt"/></button>
        </div>) : null;
        let control = (wordid, index) => {
            if (localStorage.getItem('role') === 'ADMIN') {
                return (
                    <React.Fragment>
                        <input id={`wordName${wordid}1`} className="wordName1"/>
                        <button id={`btnWord${wordid}`} className="word-up-button" type="button"
                                onClick={this.onUpdateWordWithData(wordid)}><i
                            className="far fa-check-circle"></i>
                        </button>
                        <button type="button" onClick={this.onDelete(wordid)}
                        ><i className="fas fa-trash-alt"/></button>
                        <input type="checkbox" className={`check${index} checkboxes`}/>
                        <button className="update-button" type="button"
                                onClick={this.onUpdateWord(wordid)}><i className="fas fa-edit"></i>
                        </button>
                    </React.Fragment>
                );
            }
        };

        const content = this.state.error ? <h1>404</h1> : (
            <React.Fragment>
                <div className="myRow3">
                    <div className="playlist">
                        <img alt="apple" src={image}/>
                        <div id="title" className="title">{title}</div>
                        <div id="topic" className="topic">{topic}</div>
                        <input id="title1" className="title1"/>
                        <input id="topic1" className="topic1"/>
                        <button id="updateButton" onClick={this.onUpdatePlaylistWithData}>OK</button>
                        {deleteUpdate}
                    </div>
                </div>
                <div className="myRow4">
                    {array.map((word, index) => {
                        return (
                            <div className="word" key={index}>
                                <div id={`wordName${word.id}`}
                                     className="wordName"> {index + 1}. {word.lang1} - {word.lang2} </div>
                                {control(word.id, index)}
                            </div>
                        )
                    })}
                    {form}
                </div>
            </React.Fragment>
        );
        return (
            <React.Fragment>
                {content}
            </React.Fragment>
        );
    }
}

export default withRouter(withBody(Playlist, 2));
