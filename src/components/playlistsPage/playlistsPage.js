import React, {Component} from "react";
import "./playlistsPage.css";
import ApiClient from "../../services/apiClient";
import apple from "../../images/apple.jpg";
import {withBody} from "../hoc-helpers";
import {Link} from "react-router-dom";

class PlaylistsPage extends Component{

    state = {
        imgLink: null,
        playlists: [],
        needReload: false,
        loading: true,
    };

    API = new ApiClient();

    constructor(props) {
        super(props);
        this.updatePlaylists();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.needReload !== this.state.needReload) {
            this.setState({
                needReload: false,
            });
            this.updatePlaylists();
        }
    }

    onAddPlaylist = () => {
        const l1 = document.getElementById("int1").value;
        const l2 = document.getElementById("int2").value;
        this.updatePlaylists();
        console.log(this.state.imgLink);
        if (this.state.accessToken !== null) this.API.createPlaylist(l1, l2, this.state.imgLink, localStorage.getItem('accessToken'));
        this.setState({
            needReload: true,
        });
        this.updatePlaylists();
    };

    onFileChangeHandler = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('file', e.target.files[0]);
        if (e.target.files[0].size > 2 * 1024 * 1024) {
            alert('Image to large');
            return;
        }
        this.API.uploadImage(localStorage.getItem('accessToken'), 'playlists', formData).then((body) => {
            this.setState({
                imgLink: body.imageLink,
            })
        });
    };

    updatePlaylists() {

        this.API
            .getAllPlaylists()
            .then((playlists) => {
                this.setState({
                   playlists: playlists,
                    loading: false,
                });
            });
    }


    render() {

        const add = localStorage.getItem('role') === 'ADMIN' ? <div className="cell" key="add">
            <img alt="apple" src={apple}/>
            <div className="flex-col-con">
                <div className="title"><input placeholder="Enter playlist name" id="int1"/></div>
                <div className="topic"><input placeholder="Enter playlist topic" id="int2"/></div>
                <div className="words">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group files color">
                                    <label>Upload Your File </label>
                                    <input type="file" className="form-control" name="file"
                                           onChange={this.onFileChangeHandler}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button onClick={this.onAddPlaylist}>Create</button>
                </div>
            </div>
        </div> : null;

        const content = this.state.loading ? <div className="loadingio-spinner-wedges-1kn05ey5fav">
            <div className="ldio-y4vhssos6yl">
                <div>
                    <div>
                        <div></div>
                    </div>
                    <div>
                        <div></div>
                    </div>
                    <div>
                        <div></div>
                    </div>
                    <div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div> : (
            <div className="flex-container">
                {add}

                {this.state.playlists.map((playlist) => {
                    return (
                        <div className="cell" key={playlist.id}>
                            <img alt="apple" src={playlist.image}/>
                            <div className="flex-col-con">
                                <Link to={`/playlists/${playlist.id}`}>
                                    <div className="title">{playlist.title}</div>
                                </Link>
                                <div className="topic">{playlist.topic}</div>
                                <div className="words">
                                    <ul>
                                        {playlist.words.map((word, index) => {
                                            return (
                                                <li key={index}>{word.lang1} - {word.lang2}</li>
                                            )
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    )
                })}


            </div>
        );

        return (
            <React.Fragment>
                {content}
            </React.Fragment>
        );
    }
}

export default withBody(PlaylistsPage, 1);
