import React from "react";
import "./header.css";
import {Link} from "react-router-dom";

const Header = () => {
    let exit, login, register;
    if (localStorage.getItem('accessToken')) {
        exit = <button type="button" className="acc btn btn-secondary">
            <div className="profileImg"></div>
            <div className="dropdown">
                <Link to={`/user/${localStorage.getItem('user')}/profile`}>
                    <button type="button" className="exit btn btn-secondary">My profile</button>
                </Link>
                <Link to={`/user/${localStorage.getItem('user')}`}>
                    <button type="button" className="exit btn btn-secondary">My playlists</button>
                </Link>
                <Link to="/exit/">
                    <button type="button" className="exit btn btn-secondary">Exit</button>
                </Link>
            </div>
        </button>;
    } else {
        exit = null;
    }

    if (!localStorage.getItem('accessToken')) {
        login = <Link to="/loginForm/">
            <button type="button" className="btn btn-secondary">Login</button>
        </Link>;
        register = <Link to="/registerForm/">
            <button type="button" className="btn btn-secondary">Register</button>
        </Link>;
    } else {
        login = null;
    }

    return (
        <div className="header">
            <div className="row  header-row">
                <div className="col col-lg-2">

                </div>
                <div className="col col-lg-3">
                    <Link to="/playlists/"><div className="logo"></div></Link>
                </div>
                <div className="col col-lg-6" style={{paddingRight: 4+'%'}}>
                    <div className="menu btn-group" role="group" aria-label="Basic example">
                        <Link to="/play/"><button type="button" className="btn btn-secondary">Play</button></Link>
                        <Link to="/faq/"><button type="button" className="btn btn-secondary">FAQ</button></Link>

                        {login}
                        {register}
                        {exit}
                    </div>
                </div>
                <div className="col col-lg-1">

                </div>
            </div>

        </div>
    );
};

export default Header;
