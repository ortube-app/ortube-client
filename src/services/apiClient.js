export default class ApiClient {

    _apiBase = 'https://ortube-dev.herokuapp.com/ortube/api/v1';

    postGlobalResource = async (url, data = null, headers, method, jsonNeed = false) => {
        const res = await fetch(`${this._apiBase}${url}`,
            {
                method,
                body: data ? JSON.stringify(data) : null,
                headers,
            });
        if (jsonNeed) {
            return await res.json();
        }
        return await res;
    };

    postGlobalResource2 = async (url, data = null, headers, method, jsonNeed = false) => {
        const res = await fetch(`${this._apiBase}${url}`,
            {
                method,
                body: data ? data : null,
                headers,
            });

        if (jsonNeed) {
            return await res.json();
        }
        return await res;
    };

    authorization = async (url, data, hash) => {
        const res = await fetch(`${this._apiBase}${url}`,
                                    {method: 'post',
                                        body: JSON.stringify(data),
                                        headers: {"Browser-Fingerprint": hash, "Content-Type": "application/json"}
                                    });

        return await res.json();
    };


    getResource = async (url) => {
        const res = await fetch(`${this._apiBase}${url}`, {method: 'get'});

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
                `, received ${res.status}`)
        }
        return await res.json();
    };

    getAllPlaylists = async () => {
        return await this.getResource('/playlist/');
    };

    getAllUsersPlaylists = async (token) => {
        return await this.postGlobalResource(`/secure/userPlaylist`, null, {"Authorization": `Bearer ${token}`}, 'get', true);
    };

    getPlaylist = async (id) => {
        return await this.getResource(`/playlist/${id}`);
    };

    getUserPlaylist = async (id, token) => {
        return await this.postGlobalResource(`/secure/userPlaylist/${id}`, null, {"Authorization": `Bearer ${token}`}, 'get', true);
    };

    addUserPlaylist = async (id, token) => {
        return await this.postGlobalResource(`/secure/userPlaylist/add/${id}`, null, {"Authorization": `Bearer ${token}`}, 'post', true);
    };

    deleteUserPlaylist = async (id, token) => {
        return await this.postGlobalResource(`/secure/userPlaylist/${id}`, null, {"Authorization": `Bearer ${token}`}, 'delete', false);
    };

    postWordToPlaylist = async (obj, id, token) => {
        return await this.postGlobalResource(`/secure/playlist/${id}/word`,
            obj,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'post',
            true);
    };

    postWordToUserPlaylist = async (obj, id, token) => {
        return await this.postGlobalResource(`/secure/userPlaylist/${id}/word`,
            obj,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'post',
            true);
    };

    deleteWordFromPlaylist = async (idWord, idPlaylist, token) => {
        return await this.postGlobalResource(`/secure/playlist/${idPlaylist}/word/${idWord}`,
            null,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'delete',
            false);
    };

    deleteWordFromUserPlaylist = async (idWord, idPlaylist, token) => {
        return await this.postGlobalResource(`/secure/userPlaylist/${idPlaylist}/word/${idWord}`,
            null,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'delete',
            false);
    };

    deleteSeveralWordsFromPlaylist = async (idsWord, idPlaylist, token) => {
        return await this.postGlobalResource(`/secure/playlist/${idPlaylist}/word/`,
            idsWord,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'delete',
            false);
    };

    deleteSeveralWordsFromUserPlaylist = async (idsWord, idPlaylist, token) => {
        return await this.postGlobalResource(`/secure/userPlaylist/${idPlaylist}/word/`,
            idsWord,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'delete',
            false);
    };

    createPlaylist = async (title, topic, imgLInk, token) => {
        return await this.postGlobalResource(`/secure/playlist`,
            {
                "title": title,
                "topic": topic,
                "image": imgLInk
            },
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'post',
            true);
    };

    createUserPlaylist = async (privacy, title, topic, token) => {
        return await this.postGlobalResource(`/secure/userPlaylist`,
            {
                "privacy": privacy,
                "title": title,
                "topic": topic
            },
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'post',
            true);
    };

    deletePlaylist = async (idPlaylist, token) => {
        return await this.postGlobalResource(`/secure/playlist/${idPlaylist}`,
            null,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'delete',
            false);
    };

    updatePlaylist = async (idPlaylist, data, token) => {
        return await this.postGlobalResource(`/secure/playlist/${idPlaylist}`,
            data,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'put',
            true);
    };

    updateUserPlaylist = async (idPlaylist, data, token) => {
        return await this.postGlobalResource(`/secure/userPlaylist/${idPlaylist}`,
            data,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'put',
            true);
    };

    updateWord = async (idPlaylist, data, token, idWord) => {
        return await this.postGlobalResource(`/secure/playlist/${idPlaylist}/word/${idWord}`,
            data,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'put',
            true);
    };

    updateUserWord = async (idPlaylist, data, token, idWord) => {
        return await this.postGlobalResource(`/secure/userPlaylist/${idPlaylist}/word/${idWord}`,
            data,
            {"Content-Type": "application/json", "Authorization": `Bearer ${token}`},
            'put',
            true);
    };

    login = async (obj, hash) => {
        return await this.authorization(`/auth/login`, obj, hash);
    };

    register = async (obj, hash) => {
        return await this.authorization(`/register`, obj, hash);
    };

    me = async (token) => {
        return await this.postGlobalResource(`/auth/me`,
            null,
            {"Authorization": `Bearer ${token}`},
            'get',
            true);
    };

    startQuiz = async (id, token, image = 'no-image', mode = 'lang1-lang2') => {
        return await this.postGlobalResource(`/secure/quiz/start/${id}?image=${image}&mode=${mode}`,
            null,
            {"Authorization": `Bearer ${token}`},
            'post',
            true);
    };

    activate = async (token, activationCode) => {
        return await this.postGlobalResource(`/activate/${activationCode}`,
            null,
            {"Authorization": `Bearer ${token}`},
            'post',
            false);
    };

    uploadImage = async (token, folder, file) => {
        return await this.postGlobalResource2(`/image/${folder}`,
            file,
            {"Authorization": `Bearer ${token}`},
            'post',
            true);
    }
}

